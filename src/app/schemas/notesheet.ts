import { ObjectId } from 'bson';

export interface Notesheet {
  _id: ObjectId;
  title: String;
  questions: String[];
  body: String;
  summary: String;
}
