import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'note-module',
  templateUrl: './note-module.component.html',
  styleUrls: ['./note-module.component.css'],
})
export class NoteModuleComponent implements OnInit {
  @Input()
  noteId?: String;

  constructor() {}

  ngOnInit(): void {}
}
