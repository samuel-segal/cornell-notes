import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesheetComponent } from './notesheet.component';

describe('NotesheetComponent', () => {
  let component: NotesheetComponent;
  let fixture: ComponentFixture<NotesheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NotesheetComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
