import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ObjectId } from 'bson';
import { Notesheet } from '../schemas/notesheet';
import { NotesheetService } from '../notesheet.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'notesheet',
  templateUrl: './notesheet.component.html',
  styleUrls: ['./notesheet.component.css'],
})
export class NotesheetComponent implements OnInit {
  notesheet: Notesheet | undefined;

  constructor(
    private route: ActivatedRoute,
    private notesheetService: NotesheetService
  ) {}

  ngOnInit(): void {
    this.getNoteSheet();
  }

  //TODO Change this from String to a notesheet class
  getNoteSheet(): void {
    const id = this.route.snapshot.paramMap.get('id') ?? '';
    this.notesheetService.getNotesheet(id).subscribe((ns: Notesheet) => {
      this.notesheet = ns;
    });
  }

  saveNotesheet(): void {
    console.log(this.notesheet);
    if (this.notesheet) {
      const noteObs: Observable<Notesheet> =
        this.notesheetService.editNotesheet(this.notesheet);
      noteObs.subscribe();
    }
  }
}
