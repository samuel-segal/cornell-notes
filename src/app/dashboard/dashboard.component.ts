import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Notesheet } from '../schemas/notesheet';
import { NotesheetService } from '../notesheet.service';
import { NotesheetData } from '../schemas/notesheet-data';
import { PersonalSheetsService } from '../personal-sheets.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
//We might want to put this into a service
export class DashboardComponent implements OnInit {
  constructor(public personalSheets: PersonalSheetsService) {}

  noteModules: NotesheetData[] = [];

  ngOnInit(): void {
    this.noteModules = this.personalSheets.getNotesheetModules();
    this.personalSheets.updateNotesheetModules();
  }
}
