import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Notesheet } from './schemas/notesheet';

@Injectable({
  providedIn: 'root',
})
export class NotesheetService {
  constructor(private http: HttpClient) {}

  getNotesheet(id: string): Observable<Notesheet> {
    return this.http.get<Notesheet>(`api/notes/${id}`);
  }

  editNotesheet(notesheet: Notesheet): Observable<Notesheet> {
    return this.http.post<Notesheet>('api/edit', notesheet);
  }

  //Returns notesheet id
  createNotesheet(): Observable<Notesheet> {
    //Probably not the best implementation, but its whatever
    return this.http.get<Notesheet>('api/create');
  }
}
