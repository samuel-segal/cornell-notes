import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NotesheetService } from './notesheet.service';
import { Notesheet } from './schemas/notesheet';
import { NotesheetData } from './schemas/notesheet-data';

@Injectable({
  providedIn: 'root',
})
export class PersonalSheetsService {
  noteModules: NotesheetData[] = [];

  constructor(private notesheetService: NotesheetService) {
    const storStr: string = localStorage.getItem('userNotesheets') ?? '[]';
    this.noteModules = JSON.parse(storStr);
  }

  getNotesheetModules(): NotesheetData[] {
    return this.noteModules;
  }

  createNotesheet(): void {
    this.notesheetService.createNotesheet().subscribe((sheet: Notesheet) => {
      console.log(sheet._id);
      this.noteModules.push({ id: sheet._id.toString(), title: sheet.title });
      this.saveNotesheetModules();
    });
  }

  saveNotesheetModules(): void {
    const storStr = JSON.stringify(this.noteModules);
    localStorage.setItem('userNotesheets', storStr);
  }

  updateNotesheetModules(): void {
    this.noteModules.forEach((sheet) => {
      //TODO This is overcalling. Designate a specific endpoint
      const sheetObs = this.notesheetService.getNotesheet(sheet.id);
      sheetObs.subscribe((updateSheet: Notesheet) => {
        if (updateSheet.title != sheet.title) {
          sheet.title = updateSheet.title;
          //TODO get this to finish all in one save
          this.saveNotesheetModules();
        }
      });
    });
  }
}
