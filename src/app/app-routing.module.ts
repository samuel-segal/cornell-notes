import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { NotesheetComponent } from './notesheet/notesheet.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Route[] = [
  { path: 'notesheet/:id', component: NotesheetComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
