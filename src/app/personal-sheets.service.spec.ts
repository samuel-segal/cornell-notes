import { TestBed } from '@angular/core/testing';

import { PersonalSheetsService } from './personal-sheets.service';

describe('PersonalSheetsService', () => {
  let service: PersonalSheetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonalSheetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
