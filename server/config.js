require("dotenv").config();

module.exports.port = process.env["PORT"] || 8080;
module.exports.mongoUri = process.env["MONGO"];
