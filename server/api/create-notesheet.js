const express = require("express");
const router = express.Router();

const dbClient = require("../db/client.js");

router.get("/", async (req, res) => {
  const client = await dbClient();
  const notesDb = client.db("notes");
  const noteSheets = notesDb.collection("noteSheets");

  const newNotesheet = {
    title: "New Notesheet",
    body: "",
    questions: [],
    summary: "A new notesheet",
  };
  const dbRes = await noteSheets.insertOne(newNotesheet);
  const id = dbRes.insertedId;

  const resJson = newNotesheet;
  resJson._id = id;

  console.log(resJson);

  res.json(resJson);

  await client.close();
});

module.exports = router;
