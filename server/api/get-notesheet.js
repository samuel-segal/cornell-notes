const { ObjectId } = require("bson");
const express = require("express");
const router = express.Router();

const dbClient = require("../db/client.js");

router.get("/:id", async (req, res) => {
  const id = new ObjectId(req.params.id);

  const client = await dbClient();
  const notesDb = client.db("notes");
  const noteSheets = notesDb.collection("noteSheets");

  const sheet = await noteSheets.findOne({ _id: id });
  res.json(sheet);

  await client.close();
});

module.exports = router;
