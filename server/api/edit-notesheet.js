const { ObjectId } = require("bson");
const express = require("express");
const router = express.Router();

const dbClient = require("../db/client.js");

//This is a mess. Fix this later.
//TODO MAJOR REFACTORING NECESSARY FOR DB.
router.post("/", async (req, res) => {
  const document = req.body;
  const id = ObjectId(document._id);
  delete document._id;

  console.log(document);

  const client = await dbClient();
  const db = client.db("notes");
  const notesheets = db.collection("noteSheets");

  console.log(document._id);
  //Fix this probably
  const dbres = await notesheets.updateOne({ _id: id }, { $set: document });
  console.log(dbres);

  client.close();
});

module.exports = router;
