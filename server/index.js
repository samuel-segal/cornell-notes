const express = require("express");
const bodyParser = require("body-parser");

const config = require("./config.js");
const getNoteSheet = require("./api/get-notesheet.js");
const createNoteSheet = require("./api/create-notesheet.js");
const editNoteSheet = require("./api/edit-notesheet.js");

const app = express();

app.use("/", (req, res, next) => {
  console.log(req.url);
  next();
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/*app.use("/api/notes/:id", async (req, res, next) => {
  const id = new ObjectId(req.params.id);
  const noteSheet = await getNoteSheet(id);
  res.json(noteSheet);
});*/

app.use("/api/notes", getNoteSheet);
app.use("/api/create", createNoteSheet);
app.use("/api/edit", editNoteSheet);

app.listen(config.port, () => {
  console.log(`Backend server listening on port ${config.port}`);
});
