const MongoClient = require("mongodb").MongoClient;

const config = require("../config.js");

module.exports = async function () {
  const client = new MongoClient(config.mongoUri);
  await client.connect();
  return client;
};
